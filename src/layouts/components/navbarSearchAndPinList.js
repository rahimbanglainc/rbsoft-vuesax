export default {
	actionIcon: 'StarIcon',
	highlightColor: 'warning',
	data: [
    // DASHBOARDS
		{index: 0, label: 'Analytics Dashboard', url: '/dashboard', labelIcon: 'HomeIcon', highlightAction: false},
		{index: 1, label: 'eCommerce Dashboard', url: '/dashboard/profile', labelIcon: 'HomeIcon', highlightAction: false},

    // APPS
		{index: 2, label: 'Todo', url: '/todo', labelIcon: 'CheckSquareIcon', highlightAction: true},
		{index: 3, label: 'Chat', url: '/chat', labelIcon: 'MessageSquareIcon', highlightAction: true},
		{index: 4, label: 'Blog', url: '/blog', labelIcon: 'MailIcon', highlightAction: true},
    {index: 5, label: 'Feedback', url: '/feedback', labelIcon: 'CalendarIcon', highlightAction: true},
    {index: 6, label: 'E-Commerce Shop', url: '/', labelIcon: 'ShoppingCartIcon', highlightAction: true},
    {index: 7, label: 'E-Commerce Wish List', url: '/', labelIcon: 'HeartIcon', highlightAction: false},
		{index: 8, label: 'E-Commerce Checkout', url: '/', labelIcon: 'CreditCardIcon', highlightAction: false},

    
    // PAGES
		{index: 54, label: 'Login Page', url: '/pages/login', labelIcon: 'LockIcon', highlightAction: false},
		{index: 55, label: 'Register Page', url: '/pages/register', labelIcon: 'UserPlusIcon', highlightAction: false},
		{index: 56, label: 'Forgot Password Page', url: '/pages/forgot-password', labelIcon: 'HelpCircleIcon', highlightAction: false},
		{index: 57, label: 'Reset Password Page', url: '/pages/reset-password', labelIcon: 'UnlockIcon', highlightAction: false},
		{index: 58, label: 'Lock Screen Page', url: '/pages/lock-screen', labelIcon: 'LockIcon', highlightAction: false},
		{index: 59, label: 'Coming Soon Page', url: '/pages/comingsoon', labelIcon: 'ClockIcon', highlightAction: false},
		{index: 60, label: '404 Page', url: '/pages/error-404', labelIcon: 'MonitorIcon', highlightAction: false},
		{index: 61, label: '500 Page', url: '/pages/error-500', labelIcon: 'MonitorIcon', highlightAction: false},
		{index: 62, label: 'Not Authorized Page', url: '/pages/not-authorized', labelIcon: 'XCircleIcon', highlightAction: false},
		{index: 63, label: 'Maintenance Page', url: '/pages/maintenance', labelIcon: 'MonitorIcon', highlightAction: false},
		{index: 64, label: 'Profile Page', url: '/pages/profile', labelIcon: 'UserIcon', highlightAction: false},
		{index: 65, label: 'FAQ Page', url: '/pages/faq', labelIcon: 'HelpCircleIcon', highlightAction: false},
		{index: 66, label: 'KnowledgeBase Page', url: '/pages/knowledge-base', labelIcon: 'BookIcon', highlightAction: false},
		{index: 67, label: 'Search Page', url: '/pages/search', labelIcon: 'SearchIcon', highlightAction: false},
		{index: 68, label: 'Invoice Page', url: '/pages/invoice', labelIcon: 'FileIcon', highlightAction: false},

  	]
}
