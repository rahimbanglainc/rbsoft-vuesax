/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
  ----------------------------------------------------------------------------------------
  Item Name: Vuesax Admin - VueJS Dashboard Admin Template
  Author: Abdur Rahim
  Author URL: http://rahimbangla.me
==========================================================================================*/


export default [
  {
    url: null,
    name: "Dashboard",
    tag: "2",
    tagColor: "warning",
    icon: "HomeIcon",
    i18n: "Dashboard",
    submenu: [
      {
        url: '/dashboard',
        name: "Deshboard",
        slug: "dashboard",
        i18n: "Deshboard",
      },
      {
        url: '/dashboard/profile',
        name: "profile",
        slug: "dashboard-profile",
        i18n: "Student Profile",
      },
    ]
  },
  {
    header: "Links",
    i18n: "Links",
  },
  {
    url: "/blog",
    name: "Blog",
    slug: "blog",
    icon: "MailIcon",
    i18n: "Student Blog",
  },
  {
    url: "/chat",
    name: "Chat",
    slug: "chat",
    icon: "MessageSquareIcon",
    i18n: "Student Chat",
  },
  {
    url: "/todo",
    name: "Todo",
    slug: "todo",
    icon: "CheckSquareIcon",
    i18n: "Student Todo",
  },
  {
    url: "/feedback",
    // url: null,
    name: "Feedback",
    slug: "feedback",
    icon: "CalendarIcon",
    tagColor: "success",
    i18n: "Feedback",
  },
]
